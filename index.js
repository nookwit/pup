const storageKey = "nookwit_pup";
let options = {};

function canProceed() {
  chrome.storage.sync.get(storageKey, options => {
    try {
      const settings = options.nookwit_pup;

      if (settings.domains !== undefined && settings.domains.length > 0) {
        const currentHost = window.location.hostname;
        for (let domain in settings.domains) {
          const url = new URL(domain);
          const host = url.hostname;
          if (host === currentHost) {
            return false;
          }
        }
      }
    } catch (e) {
      return false;
    }
  });

  return true;
}

function execute(processed) {
  const x = document.activeElement;

  if (x.tagName.toLowerCase() === "input" && x.type.toLowerCase() === "text") {
    return;
  }

  const inputs = document.querySelectorAll("input[type=text]");

  const input = inputs[0];

  if (input !== undefined) {
    console.log("setting focus", input);
    input.focus();
  }
}

function process() {
  if (canProceed() === true) {
    execute();
  }
}

process();
