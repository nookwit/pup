const wordLimit = 250;
const domainLimit = 100;

function tableBody(id) {
  return document.getElementById(id).getElementsByTagName("tbody")[0];
}

function tableBodyRows(id) {
  return tableBody(id).querySelectorAll("tr");
}

function clearLists() {
  tableBody("domains").innerHTML = "";
  present();
}

function clearDomains() {
  const r = confirm("Are you sure you want to clear all domains?");

  if (r == true) {
    getSettings(function (wd) {
      wd.domains = [];
      setSettings(wd);
      clearLists();
    });
  }
}

function addDomainRow(ndx, domain) {
  const tbl = tableBody("domains");
  const row = tbl.insertRow();
  if (domain && domain.length > 0) {
    try {
      insertCellWithTextInput(row, ndx, "domain", domain);
    } catch (e) {
      alert(`Invalid domain entered: ${domain}. Please retry with valid domain name.`);
    }
  } else {
    insertCellWithTextInput(row, ndx, "domain", domain);
  }
}

function insertCellWithTextInput(row, ndx, prefix, val) {
  const newCell = row.insertCell();
  const inInput = document.createElement("input");
  inInput.type = "text";
  inInput.id = `${prefix}${ndx}`;
  inInput.value = `${val}`;
  newCell.appendChild(inInput);
}

function newSettings() {
  return { domains: [] };
}

function saveSettings() {
  const settings = newSettings();

  const domains = tableBodyRows("domains");
  for (let i = 0; i < domains.length; i++) {
    const val = document.getElementById(`domain${i}`).value;
    if (val && val.length > 0) {
      try {
        const url = new URL(val);
        settings.domains.push(url.toString());
      } catch (e) {
        alert("Error saving domain, please ensure proper format of https://mydomain.com", e);
        return false;
      }
    }
  }

  setSettings(settings);
  return true;
}

function getSettings(callback) {
  chrome.storage.sync.get("nookwit_pup", function (settings) {
    if (settings && settings.wordDomination) {
      callback(settings.wordDomination);
    } else {
      callback(newSettings());
    }
  });
}

function addDomain() {
  const rows = tableBodyRows("domains");

  if (rows && rows.length <= domainLimit) {
    if (saveSettings()) {
      addDomainRow(rows.length, "");
    }
  } else {
    alert(`Reached limit of ${domainLimit} domains.`);
  }
}

function present() {
  getSettings(function (wd) {
    if (wd.domains && wd.domains.length > 0) {
      for (let i = 0; i < wd.domains.length; i++) {
        addDomainRow(i, wd.domains[i]);
      }
    } else {
      addDomainRow(0, "");
    }
  });
}

function setSettings(settings) {
  chrome.storage.sync.set({ nookwit_pup: settings }, function () {
    // Update status to let user know options were saved.
    var status = document.getElementById("status");
    status.textContent = "Options saved.";
    setTimeout(function () {
      status.textContent = "";
    }, 3000);
  });
}

document.addEventListener("DOMContentLoaded", present);
document.getElementById("save").addEventListener("click", saveSettings);
document.getElementById("addDomain").addEventListener("click", addDomain);
document.getElementById("clearDomains").addEventListener("click", clearDomains);
